﻿using PortalGuitarFlash.DDD.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalGuitarFlash.Infra.Data.EntityConfig
{
    public class AuthorConfiguration: EntityTypeConfiguration<Author>
    {

        public AuthorConfiguration()
        {
            Property(p => p.Name).IsRequired()
                .HasColumnAnnotation(
                IndexAnnotation.AnnotationName,
                new IndexAnnotation(
                    new IndexAttribute() { IsUnique = true }));

            HasMany<ExternalSong>(p => p.ExternalSongs);

        }

    }
}
