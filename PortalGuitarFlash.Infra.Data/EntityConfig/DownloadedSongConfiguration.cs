﻿using PortalGuitarFlash.DDD.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;

namespace PortalGuitarFlash.Infra.Data.EntityConfig
{
    public class DownloadedSongConfiguration: EntityTypeConfiguration<DownloadedSong>
    {
        public DownloadedSongConfiguration()
        {
            HasRequired(p => p.ExternalSong);
            HasRequired(p => p.User);
        }

    }
}
