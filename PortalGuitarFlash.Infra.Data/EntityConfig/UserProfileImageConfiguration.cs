﻿using PortalGuitarFlash.DDD.Entities;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;

namespace PortalGuitarFlash.Infra.Data.EntityConfig
{
    public class UserProfileImageConfiguration: EntityTypeConfiguration<UserProfileImage>
    {

        public UserProfileImageConfiguration()
        {
            Property(p => p.ID).IsRequired();
            Property(p => p.Image).IsRequired();
            Property(p => p.Type).IsRequired();
            HasRequired(p => p.Owner);
        }

    }
}
