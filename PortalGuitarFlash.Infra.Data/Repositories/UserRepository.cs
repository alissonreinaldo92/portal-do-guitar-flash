﻿using PortalGuitarFlash.DDD.Entities;
using PortalGuitarFlash.DDD.Interfaces.Repositories;
using System.Linq;

namespace PortalGuitarFlash.Infra.Data.Repositories
{
    public class UserRepository: BaseRepository<User>, IUserRepository
    {
        public User GetByEmail(string email)
        {
            return Db.Set<User>().Where(u => u.Email == email).SingleOrDefault();
        }

    }
}
