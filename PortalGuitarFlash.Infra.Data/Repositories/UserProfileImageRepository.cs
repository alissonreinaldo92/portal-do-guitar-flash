﻿using PortalGuitarFlash.DDD.Entities;
using PortalGuitarFlash.DDD.Interfaces.Repositories;

namespace PortalGuitarFlash.Infra.Data.Repositories
{
    public class UserProfileImageRepository : BaseRepository<UserProfileImage>, IUserProfileImageRepository
    {
    }
}
