﻿using PortalGuitarFlash.DDD.Entities;
using PortalGuitarFlash.DDD.Interfaces.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace PortalGuitarFlash.Infra.Data.Repositories
{
    public class ExternalSongRepository : BaseRepository<ExternalSong>, IExternalSongRepository
    {

        public IEnumerable<ExternalSong> FindByName(string name)
        {
            return Db.Set<ExternalSong>().Where(s => s.Name == name);
        }

        protected override IList<ExternalSong> Load(IEnumerable<ExternalSong> entities)
        {
            var songs = entities.Select(s => new
            {
                Song = s,
                DownloadCount = s.DownloadedSongs.Count()
            });
            foreach (var s in songs)
                s.Song.SetDownloadCount(s.DownloadCount);
            return songs.Select(s => s.Song).ToList();
        }

    }
}
