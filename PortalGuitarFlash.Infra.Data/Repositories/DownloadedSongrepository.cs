﻿using PortalGuitarFlash.DDD.Entities;
using PortalGuitarFlash.DDD.Interfaces.Repositories;

namespace PortalGuitarFlash.Infra.Data.Repositories
{
    public class DownloadedSongRepository : BaseRepository<DownloadedSong>, IDownloadedSongRepository
    {
    }
}
