﻿using PortalGuitarFlash.DDD.Interfaces.Repositories;
using PortalGuitarFlash.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using PortalGuitarFlash.Infra.Data.Repositories.Helpers;
using PortalGuitarFlash.DDD.Interfaces;
using PortalGuitarFlash.DDD.Helpers;

namespace PortalGuitarFlash.Infra.Data.Repositories
{
    public class BaseRepository<TEntity>: IBaseRepository<TEntity> where TEntity: class
    {

        protected static readonly PortalGuitarFlashContext Db = new PortalGuitarFlashContext();

        public void Add(TEntity obj, bool SaveChanges = true)
        {
            Db.Set<TEntity>().Add(obj);
            if (SaveChanges)
                Db.SaveChanges();
        }
        
        public TEntity GetById(int ID)
        {
            return Db.Set<TEntity>().Find(ID);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return this.Load(Db.Set<TEntity>());
        }

        public CollectionPage<TEntity> GetAllPaged(int page, int itemsPerPage, IEntitySearchBuilder<TEntity> builder)
        {
            var pagedEntities = new CollectionPage<TEntity>();
            pagedEntities.Items = this.Load(builder.Build(Db.Set<TEntity>()).Skip(itemsPerPage * (page - 1)).Take(itemsPerPage));
            pagedEntities.CurrentPage = page;
            pagedEntities.TotalItems = Db.Set<TEntity>().Count();
            pagedEntities.ItemsPerPage = itemsPerPage;
            return pagedEntities;
        }

        protected virtual IList<TEntity> Load(IEnumerable<TEntity> entities)
        {
            return entities.ToList();
        }

        public void SaveChanges()
        {
            Db.SaveChanges();
        }

        public void Update(TEntity obj, bool SaveChanges = true)
        {
            Db.Entry(obj).State = EntityState.Modified;
            if (SaveChanges)
                Db.SaveChanges();
        }

        public void Remove(TEntity obj, bool SaveChanges = true)
        {
            Db.Set<TEntity>().Remove(obj);
            if (SaveChanges)
                Db.SaveChanges();
        }

        public void Dispose()
        {
            Db.Dispose();
        }

    }
}
