﻿using AutoMapper;
using PortalGuitarFlash.DDD.Entities;
using PortalGuitarFlash.Web.ViewModels;
using PortalGuitarFlash.Web.ViewModels.Song;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PortalGuitarFlash.Web.Helpers;
using PortalGuitarFlash.Web.ViewModels.User;
using PortalGuitarFlash.DDD.Helpers;

namespace PortalGuitarFlash.Web.AutoMapper
{
    public class ViewModelToDomainMappingProfile: Profile
    {

        public override string ProfileName
        {
            get
            {
                return "ViewModelToDomainMappingProfile";
            }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<UserViewModel, User>()
                .ForMember(u => u.Active, opt => { opt.Ignore(); opt.UseDestinationValue(); })
                .IgnoreAllPropertiesWithAnInaccessibleSetter()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<ProfileViewModel, User>()
                .ForMember(u => u.Active, opt => { opt.Ignore(); opt.UseDestinationValue(); })
                .IgnoreAllPropertiesWithAnInaccessibleSetter();

            Mapper.CreateMap<ExternalSongEditViewModel, ExternalSong>().IgnoreAllPropertiesWithAnInaccessibleSetter();
            Mapper.CreateMap<AuthorViewModel, Author>().IgnoreAllPropertiesWithAnInaccessibleSetter();
            Mapper.CreateMap<ChangeLogViewModel, ChangeLog>().IgnoreAllPropertiesWithAnInaccessibleSetter();
            Mapper.CreateMap(typeof(CollectionPage<>), typeof(CollectionPage<>));
        }

    }
}