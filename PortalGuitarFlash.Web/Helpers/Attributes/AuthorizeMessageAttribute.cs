﻿using System;
using System.Web.Mvc;

namespace PortalGuitarFlash.Web.Helpers.Attributes
{
    public class AuthorizeMessageAttribute : AuthorizeAttribute
    {
        private bool _override = false;
        public string Message { get; set; }
        public bool Override { get { return _override; } set { _override = value; } }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var action = filterContext.ActionDescriptor;
            if (!this.Override && action.IsDefined(typeof(AuthorizeAttribute), true))
                return;
            base.OnAuthorization(filterContext);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Controller.TempData["AuthorizationMessage"] = this.Message;
            base.HandleUnauthorizedRequest(filterContext);
        }

    }
}