﻿using PortalGuitarFlash.DDD.Interfaces.Repositories;
using PortalGuitarFlash.Infra.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PortalGuitarFlash.Web.ViewModels;
using System.Web.Mvc;

namespace PortalGuitarFlash.Web.Helpers
{
    public static class Helpers
    {

        private static readonly IUserRepository repo = new UserRepository();

        public static PortalGuitarFlash.DDD.Entities.User GetLoggedUser()
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                return null;
            UserManager<ApplicationUser> manager = GetUserManager();
            ApplicationUser user = manager.FindById(HttpContext.Current.User.Identity.GetUserId());
            if (user == null)
                return null;
            return repo.GetByEmail(user.Email);
        }

        public static UserManager<ApplicationUser> GetUserManager()
        {
            return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
        }

        public static bool IsAuthenticated()
        {
            return GetLoggedUser() != null;
        }

    }
}