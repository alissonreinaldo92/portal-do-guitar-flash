﻿using AutoMapper;
using PortalGuitarFlash.DDD.Entities;
using PortalGuitarFlash.DDD.Interfaces.Repositories;
using PortalGuitarFlash.Infra.Data.Repositories;
using PortalGuitarFlash.Web.ViewModels;
using PortalGuitarFlash.Web.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PortalGuitarFlash.Web.Controllers
{
    public class UserController : Controller
    {

        public static readonly IUserRepository repo = new UserRepository();

        public ActionResult Index()
        {
            if (TempData["ErrorMessage"] != null)
                ViewBag.ErrorMessage = TempData["ErrorMessage"].ToString();
            IEnumerable<User> users = repo.GetAll().OrderByDescending(a => a.GetCharterPoints()).ThenBy(a => a.RegisterDate).Take(50);
            return View(Mapper.Map<IEnumerable<UserViewModel>>(users));
        }

        [HttpGet]
        [Route("profile/edit", Name = "Profile.Edit")]
        public ActionResult Edit()
        {
            User user = Helpers.Helpers.GetLoggedUser();
            if (user == null)
            {
                TempData["ErrorMessage"] = "Necessário estar logado para editar o perfil!";
                return RedirectToAction("Index");
            }
            ProfileViewModel model = Mapper.Map<ProfileViewModel>(user);
            return View(model);
        }

        [HttpPost]
        [Route("profile/edit", Name = "Profile.Save")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProfileViewModel profile)
        {
            if (ModelState.IsValid)
            {
                User current = repo.GetById(profile.ID);
                User user = Mapper.Map<ProfileViewModel, User>(profile, current);
                repo.Update(user);
                return RedirectToAction("ShowProfile");
            }
            return View(profile);
        }

        [Route("profile/{id:int?}", Name="Profile")]
        public ActionResult ShowProfile(int id = 0)
        {
            User user = id == 0 ? Helpers.Helpers.GetLoggedUser() : repo.GetById(id);
            if (user == null)
            {
                if (id != 0)
                    TempData["ErrorMessage"] = String.Format("Usuário de ID {0} não localizado!", id);
                return RedirectToAction("Index");
            }
            ProfileViewModel model = Mapper.Map<ProfileViewModel>(user);
            //model.ProfileImageUrl = Url.Action("UserProfileImage", "Image", new { userId = user.ID });
            return View("profile", model);
        }

    }
}
