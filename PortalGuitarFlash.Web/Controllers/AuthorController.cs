﻿using AutoMapper;
using PortalGuitarFlash.DDD.Entities;
using PortalGuitarFlash.DDD.Interfaces.Repositories;
using PortalGuitarFlash.Infra.Data.Repositories;
using PortalGuitarFlash.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PortalGuitarFlash.Web.Controllers
{
    public class AuthorController : Controller
    {

        protected static readonly IAuthorRepository repo = new AuthorRepository();

        public ActionResult Index()
        {
            IEnumerable<Author> authors = repo.GetAll().OrderByDescending(a => a.Name.Length).Take(10);
            return View(Mapper.Map<IEnumerable<AuthorViewModel>>(authors));
        }

        public ActionResult Edit(int id)
        {
            Author author = repo.GetById(id);
            if (author == null)
                return HttpNotFound();
            AuthorViewModel a = Mapper.Map<AuthorViewModel>(author);
            return View(a);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AuthorId,Name")] AuthorViewModel author)
        {
            if (ModelState.IsValid)
            {
                Author current = repo.GetById(author.ID);
                Author a = Mapper.Map<AuthorViewModel, Author>(author, current);
                repo.Update(a, true);
                return RedirectToAction("Index");
            }
            return View(author);
        }

    }
}
