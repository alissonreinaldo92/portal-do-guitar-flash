﻿using AutoMapper;
using PortalGuitarFlash.DDD.Entities;
using PortalGuitarFlash.DDD.Helpers;
using PortalGuitarFlash.DDD.Interfaces.Repositories;
using PortalGuitarFlash.Infra.CrossCutting.EntitySearchBuilder.Orderers;
using PortalGuitarFlash.Infra.Data.Repositories;
using PortalGuitarFlash.Web.Helpers.Attributes;
using PortalGuitarFlash.Web.ViewModels;
using PortalGuitarFlash.Web.ViewModels.Song;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace PortalGuitarFlash.Web.Controllers
{
    [AuthorizeMessage]
    public class SongController : Controller
    {

        public static readonly IExternalSongRepository repo = new ExternalSongRepository();

        [AllowAnonymous]
        public ActionResult Index(CollectionPageViewModel<ExternalSongViewModel> collectionPage = null)
        {

            PortalGuitarFlash.DDD.Entities.User user = Helpers.Helpers.GetLoggedUser();
            ViewBag.LoggedUserId = (user == null) ? 0 : user.ID;

            int page = collectionPage == null ? 1 : collectionPage.Pagina;
            int itemsPerPage = collectionPage == null ? 10 : collectionPage.Quantidade;
            string text = collectionPage == null ? "" : collectionPage.Texto;
            var builder = new SongListOrderer() { SearchText = text, SortingOrder = DDD.Enum.SortingType.Ascending };
            var paged = repo.GetAllPaged(page, itemsPerPage, builder);
            var songs = Mapper.Map<CollectionPageViewModel<ExternalSongViewModel>>(paged);
            return View(songs);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [AuthorizeMessage(Message="Para baixar uma música, é necessário estar logado!", Override=true)]
        public ActionResult Download(int id)
        {
            ExternalSong song = repo.GetById(id);
            if (song == null)
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            return Redirect(song.DownloadUrl);
        }

        public ActionResult Edit(int id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
