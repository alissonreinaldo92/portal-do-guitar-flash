﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PortalGuitarFlash.DDD.Entities;
using PortalGuitarFlash.Infra.Data.Context;
using PortalGuitarFlash.DDD.Interfaces.Repositories;
using PortalGuitarFlash.Infra.Data.Repositories;
using PortalGuitarFlash.Web.ViewModels;
using AutoMapper;

namespace PortalGuitarFlash.Web.Controllers
{
    [Authorize]
    public class ChangeLogController : Controller
    {

        protected static readonly IChangeLogRepository repo = new ChangeLogRepository();

        [AllowAnonymous]
        public ActionResult Index()
        {
            IEnumerable<ChangeLog> progresses = repo.GetAll().OrderByDescending(w => w.StartDate);
            return View(Mapper.Map<IEnumerable<ChangeLogViewModel>>(progresses));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Type,Description,Status,StartDate")] ChangeLogViewModel changeLog)
        {
            if (ModelState.IsValid)
            {
                ChangeLog c = Mapper.Map<ChangeLog>(changeLog);
                repo.Add(c);
                return RedirectToAction("Index");
            }

            return View(changeLog);
        }

        public ActionResult Edit(int id)
        {
            ChangeLog changeLog = repo.GetById(id);
            if (changeLog == null)
                return HttpNotFound();
            ChangeLogViewModel vm = Mapper.Map<ChangeLogViewModel>(changeLog);

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ChangeLogId,Type,Description,Status,StartDate")] ChangeLogViewModel changeLog)
        {
            if (ModelState.IsValid)
            {
                ChangeLog current = repo.GetById(changeLog.ID);
                ChangeLog c = Mapper.Map<ChangeLogViewModel, ChangeLog>(changeLog, current);
                repo.Update(c);
                return RedirectToAction("Index");
            }
            return View(changeLog);
        }

        public ActionResult Delete(int id)
        {
            ChangeLog changeLog = repo.GetById(id);
            if (changeLog == null)
                return HttpNotFound();
            ChangeLogViewModel vm = Mapper.Map<ChangeLogViewModel>(changeLog);
            return View(vm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ChangeLog changeLog = repo.GetById(id);
            repo.Remove(changeLog);
            return RedirectToAction("Index");
        }

    }
}
