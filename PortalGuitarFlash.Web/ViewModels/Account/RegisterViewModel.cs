﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PortalGuitarFlash.Web.ViewModels.Account
{

    public class RegisterViewModel
    {
        [Required(ErrorMessage="Por favor, digite um e-mail.")]
        [EmailAddress(ErrorMessage="O e-mail digitado não é válido.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Por favor, digite uma senha.")]
        [StringLength(100, ErrorMessage = "A {0} deve ter ao menos {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Senha")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Senha")]
        [Compare("Password", ErrorMessage = "As duas senhas são diferentes.")]
        public string ConfirmPassword { get; set; }
    }

}