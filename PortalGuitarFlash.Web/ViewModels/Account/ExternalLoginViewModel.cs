﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PortalGuitarFlash.Web.ViewModels.Account
{
    public class ExternalLoginViewModel
    {
        [Required(ErrorMessage = "Por favor, digite seu Nome.")]
        [Display(Name = "Nome")]
        public string Name { get; set; }
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }
}