﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PortalGuitarFlash.Web.ViewModels.Account
{

    public class LoginViewModel
    {
        [Required(ErrorMessage = "Por favor, digite um e-mail.")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "O e-mail digitado não é válido.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Por favor, digite sua senha.")]
        [DataType(DataType.Password)]
        [Display(Name = "Senha")]
        public string Password { get; set; }

        [Display(Name = "Lembrar-me?")]
        public bool RememberMe { get; set; }
    }

}