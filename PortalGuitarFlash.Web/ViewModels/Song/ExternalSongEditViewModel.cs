﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PortalGuitarFlash.Web.ViewModels.Song
{
    public class ExternalSongEditViewModel
    {
       [Key]
        [Required(ErrorMessage = "Por favor, digite um nome para a música!")]
        [MaxLength(100, ErrorMessage = "O nome da música deve ter no máximo {0} caracteres!")]
        [DisplayName("Nome")]
        public string Name { get; set; }
        public int OwnerID { get; set; }
        [Required(ErrorMessage = "Por favor, digite um nome para a banda/autor!")]
        [MaxLength(100, ErrorMessage = "O nome da banda/autor deve ter no máximo {0} caracteres!")]
        [DisplayName("Autor")]
        public string AuthorName { get; set; }
        [Required(ErrorMessage = "Por favor, selecione um level para a música!")]
        public float Level { get; set; }
        [Required(ErrorMessage = "Por favor, preencha um link para download da chart!")]
        [Url(ErrorMessage = "O link para download preenchido não é válido!")]
        [DisplayName("Url")]
        public string DownloadUrl { get; set; }
        [DisplayName("Vídeo")]
        [Url(ErrorMessage = "O link para o vídeo preenchido não é válido!")]
        public string VideoUrl { get; set; }
    }
}