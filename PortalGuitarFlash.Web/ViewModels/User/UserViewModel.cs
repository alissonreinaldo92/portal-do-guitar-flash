﻿using PortalGuitarFlash.Web.ViewModels.Song;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PortalGuitarFlash.Web.ViewModels
{
    public class UserViewModel
    {
        [Key]
        public int ID { get; set; }
        [Required(ErrorMessage = "Por favor, digite um nome!")]
        [MaxLength(255, ErrorMessage = "O nome deve ter até {0} caracteres!")]
        [DisplayName("Nome")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Por favor, digite um email!")]
        [MaxLength(255, ErrorMessage = "O email deve ter até {0} caracteres!")]
        public string Email { get; set; }
        [DisplayName("CP")]
        public int CharterPoints { get; set; }
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm}", ApplyFormatInEditMode = true)]
        public DateTime RegisterDate { get; set; }
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm}", ApplyFormatInEditMode = true)]
        public DateTime UpdateDate { get; set; }
        public IEnumerable<ExternalSongViewModel> ExternalSongs { get; set; }
    }
}