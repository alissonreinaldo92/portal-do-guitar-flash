﻿using PortalGuitarFlash.Web.ViewModels.Song;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PortalGuitarFlash.Web.ViewModels.User
{
    public class ProfileViewModel
    {

        [Key]
        public int ID { get; set; }
        [Required(ErrorMessage = "Por favor, preencha seu nome!")]
        [MaxLength(100, ErrorMessage = "Seu nome deve conter no máximo {0} caracteres!")]
        [Display(Name="Nome")]
        public string Name { get; set; }
        [MaxLength(4000, ErrorMessage = "A descrição deve conter no máximo {0} caracteres!")]
        [Display(Name = "Descrição")]
        public string Description { get; set; }
        [MaxLength(100, ErrorMessage = "O título deve conter no máximo {0} caracteres!")]
        [Display(Name = "Título do Perfil")]
        public string DescriptionTitle { get; set; }
        [MaxLength(100, ErrorMessage = "O local deve conter no máximo {0} caracteres!")]
        [Display(Name = "Local")]
        public string Local { get; set; }
        public string Age { get; set; }
        [DataType(DataType.Date, ErrorMessage = "O valor digitado não é uma data válida!")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Data de Nascimento")]
        public DateTime? Birthday { get; set; }
        [MaxLength(100, ErrorMessage = "Seu skype deve conter no máximo {0} caracteres!")]
        public string Skype { get; set; }
        [MaxLength(100, ErrorMessage = "Seu Twitter deve conter no máximo {0} caracteres!")]
        [Display(Name = "Twitter")]
        [Url(ErrorMessage = "O link do seu Twitter não é uma URL válida!")]
        public string TwitterUrl { get; set; }
        [MaxLength(100, ErrorMessage = "Seu facebook deve conter no máximo {0} caracteres!")]
        [Display(Name = "Facebook")]
        [Url(ErrorMessage = "O link do seu Facebook não é uma URL válida!")]
        public string FacebookUrl { get; set; }
        [MaxLength(100, ErrorMessage = "Seu Google+ deve conter no máximo {0} caracteres!")]
        [Display(Name = "Google+")]
        [Url(ErrorMessage = "O link do seu Google+ não é uma URL válida!")]
        public string GooglePlusUrl { get; set; }
        public IEnumerable<ExternalSongViewModel> ExternalSongs { get; set; }


    }
}