﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PortalGuitarFlash.Web.ViewModels
{
    public class ChangeLogViewModel
    {
        
        [Key]
        public int ID { get; set; }
        [Required(ErrorMessage = "Por favor, digite o tipo!")]
        [DisplayName("Tipo")]
        public string Type { get; set; }
        [Required(ErrorMessage = "Por favor, digite a descrição!")]
        [DisplayName("Descrição")]
        [MaxLength(100, ErrorMessage = "O nome da música deve ter no máximo {0} caracteres!")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Por favor, digite um status!")]
        public string Status { get; set; }
        [Required(ErrorMessage = "Por favor, preencha a data de início!")]
        [DisplayName("Data de Início")]
        [DataType(DataType.Date, ErrorMessage = "O valor digitado não é uma data válida!")]
        public DateTime StartDate { get; set; }
    }
}