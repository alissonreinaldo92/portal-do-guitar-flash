﻿using PortalGuitarFlash.DDD.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PortalGuitarFlash.Web.ViewModels
{
    public class CollectionPageViewModel<TEntity>
    {
        // URL PARAMETERS
        [Range(1, Int16.MaxValue, ErrorMessage = "A página não pode ser zero ou número negativo")]
        public int Pagina { get; set; }
        [DisplayName("Quantidade por página")]
        [Range(1, 100, ErrorMessage = "Por favor, selecione um valor de {0} a {1}")]
        public int Quantidade { get; set; }
        [DisplayName("Buscar por")]
        public string Texto { get; set; }


        public IEnumerable<TEntity> Items { get; set; }

        public int TotalItems { get; set; }
        public int TotalPages { get; set; }

    }
}