﻿using System.Web;
using System.Web.Optimization;

namespace PortalGuitarFlash.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/Core/jquery-{version}.js",
                        "~/bootstrap/js/bootstrap.js",
                        "~/Scripts/Core/site.js",
                        "~/Scripts/Core/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/Core/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/Core/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/user").Include(
                        "~/Scripts/User/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/scrolling-nav").Include(
                        "~/Scripts/Core/jquery.easing.min.js",
                        "~/Scripts/Core/jquery.scrollUp.js",
                        "~/Scripts/Core/scrolling-nav.js"));

            bundles.Add(new ScriptBundle("~/bundles/markdown").Include(
                        "~/third-party/bootstrap-markdown/js/bootstrap-markdown.js",
                        "~/third-party/bootstrap-markdown/locale/bootstrap-markdown.fr.js",
                        "~/third-party/bootstrap-markdown/js/markdown.js",
                        "~/third-party/bootstrap-markdown/js/configure.js"));

            // STYLES - CSS

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/bootstrap/css/bootstrap.css",
                      "~/bootstrap/css/bootstrap-theme.css",
                      "~/Content/landing-page.css"));

            bundles.Add(new StyleBundle("~/Content/user").Include(
                      "~/Content/user/*.css"));

            bundles.Add(new StyleBundle("~/Content/markdown").Include(
                      "~/third-party/bootstrap-markdown/css/bootstrap-markdown.min.css"));

            bundles.Add(new StyleBundle("~/Content/timeline").Include(
                      "~/Content/shared/timeline.css"));

            bundles.Add(new StyleBundle("~/Content/fa").Include(
                      "~/font-awesome/css/font-awesome.css"));

        }
    }
}
