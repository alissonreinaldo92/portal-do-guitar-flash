﻿using PortalGuitarFlash.DDD.Enum;
using PortalGuitarFlash.DDD.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PortalGuitarFlash.Infra.CrossCutting.EntitySearchBuilder
{
    public abstract class EntitySearchBuilder<TEntity> : IEntitySearchBuilder<TEntity>
    {

        public string SearchText { get; set; }
        public SortingType SortingOrder { get; set; }

        public abstract IOrderedEnumerable<TEntity> Build(IEnumerable<TEntity> entities);

    }
}
