﻿using PortalGuitarFlash.DDD.Interfaces;
using System;
using System.Collections.Generic;

namespace PortalGuitarFlash.Infra.CrossCutting.EntitySearchBuilder
{

    /// <summary>
    /// Holds relevant information related to a page of a collection of information.
    /// </summary>
    public class CollectionPage<T>
    {

        /// <summary>
        /// The current page.
        /// </summary>
        public int CurrentPage { get; set; }

        /// <summary>
        /// A page of items.
        /// </summary>
        public IEnumerable<T> Items { get; set; }

        /// <summary>
        /// Total number of items, regardless of page.
        /// </summary>
        public int TotalItems { get; set; }

        /// <summary>
        /// The number of items that should be shown per page.
        /// </summary>
        public int ItemsPerPage { get; set; }

        private int _totalPages = -1;

        public IEntitySearchBuilder<T> Orderer { get; set; }

        /// <summary>
        /// The total number of pages.
        /// </summary>
        public int GetTotalPages()
        {
            if (_totalPages == -1)
                _totalPages = (int)(Math.Ceiling((Decimal)this.TotalItems / (Decimal)this.ItemsPerPage));
            return _totalPages;
        }

    }
}
