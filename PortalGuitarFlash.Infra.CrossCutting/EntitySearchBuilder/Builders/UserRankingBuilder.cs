﻿using PortalGuitarFlash.DDD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PortalGuitarFlash.Infra.CrossCutting.EntitySearchBuilder.Orderers
{
    public class UserRankingOrderer : EntitySearchBuilder<User>
    {
        public override IOrderedEnumerable<User> Build(IEnumerable<User> entities)
        {
            return entities.OrderByDescending(u => u.GetCharterPoints()).ThenByDescending(u => u.RegisterDate);
        }
    }
}
