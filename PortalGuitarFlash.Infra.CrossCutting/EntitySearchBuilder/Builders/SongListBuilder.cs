﻿using PortalGuitarFlash.DDD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PortalGuitarFlash.Infra.CrossCutting.EntitySearchBuilder.Orderers
{
    public class SongListOrderer : EntitySearchBuilder<ExternalSong>
    {
        public override IOrderedEnumerable<ExternalSong> Build(IEnumerable<ExternalSong> entities)
        {
            if (this.SearchText != "")
                return entities.Where(s => s.Name.Contains(this.SearchText) || s.Author.Name.Contains(this.SearchText)).OrderBy(s => s.Name);
            return entities.OrderByDescending(s => s.RegisterDate);
        }
    }
}
