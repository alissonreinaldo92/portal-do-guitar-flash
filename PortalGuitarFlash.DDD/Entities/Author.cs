﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PortalGuitarFlash.DDD.Entities
{
    public class Author
    {
        public int ID { get; protected set; }
        public string Name { get; set; }
        public bool Active { get; protected set; }
        public DateTime RegisterDate { get; protected set; }
        public DateTime? UpdateDate { get; protected set; }
        public virtual ICollection<ExternalSong> ExternalSongs { get; protected set; }
    }
}
