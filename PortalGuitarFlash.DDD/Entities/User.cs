﻿using System;
using System.Collections.Generic;

namespace PortalGuitarFlash.DDD.Entities
{
    public class User
    {
        public int ID { get; protected set; }
        public string Email { get; protected set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DescriptionTitle { get; set; }
        public string Local { get; set; }
        public DateTime? Birthday { get; set; }
        public string Skype { get; set; }
        public string TwitterUrl { get; set; }
        public string FacebookUrl { get; set; }
        public string GooglePlusUrl { get; set; }
        public bool Active { get; protected set; }
        public DateTime RegisterDate { get; protected set; }
        public DateTime UpdateDate { get; protected set; }
        public virtual ICollection<ExternalSong> ExternalSongs { get; protected set; }
        public virtual ICollection<UserProfileImage> Images { get; protected set; }

        public User()
        {

        }

        public User(string email)
        {
            this.Email = email;
        }

        public int? GetAge()
        {
            if (!this.Birthday.HasValue)
                return null;
            DateTime today = DateTime.Today;
            int age = today.Year - this.Birthday.Value.Year;
            if (this.Birthday > today.AddYears(-age))
                age--;
            return age;
        }

        public int GetCharterPoints()
        {
            return 0;
        }

    }
}
