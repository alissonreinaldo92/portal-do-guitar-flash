﻿using System;
using System.Text.RegularExpressions;

namespace PortalGuitarFlash.DDD.Entities
{

    public class EmbeddedVideo
    {
        public string ID { get; set; }
        public string EmbeddedUrl { get; set; }
        public string Source { get; set; }
        public string Url { get; set; }

        public static readonly Regex VimeoVideoRegex = new Regex(@"vimeo\.com/(?:.*#|.*/videos/)?([0-9]+)", RegexOptions.IgnoreCase | RegexOptions.Multiline);
        public static readonly Regex YoutubeVideoRegex = new Regex(@"youtu(?:\.be|be\.com)/(?:(.*)v(/|=)|(.*/)?)([a-zA-Z0-9-_]+)", RegexOptions.IgnoreCase);


        public EmbeddedVideo(string url)
        {
            this.Url = url ?? string.Empty;

            Match youtubeMatch = YoutubeVideoRegex.Match(this.Url);
            Match vimeoMatch = VimeoVideoRegex.Match(this.Url);

            string id = string.Empty;
            string source = string.Empty;
            string embeddedUrl = string.Empty;

            if (youtubeMatch.Success)
            {
                this.ID = youtubeMatch.Groups[4].Value;
                this.Source = "Youtube";
                this.EmbeddedUrl = String.Concat("https://www.youtube.com/embed/", this.ID);
            }
            else if (vimeoMatch.Success)
            {
                this.ID = vimeoMatch.Groups[4].Value;
                this.Source = "Vimeo";
                this.EmbeddedUrl = String.Concat("https://player.vimeo.com/video/",this.ID);
            }
        }
       
    }

}
