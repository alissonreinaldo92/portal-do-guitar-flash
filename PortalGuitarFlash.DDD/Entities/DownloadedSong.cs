﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalGuitarFlash.DDD.Entities
{
    public class DownloadedSong
    {
        public int ID { get; protected set; }
        public virtual ExternalSong ExternalSong { get; set; }
        public virtual User User { get; set; }
        public DateTime DownloadDate { get; protected set; }

    }
}
