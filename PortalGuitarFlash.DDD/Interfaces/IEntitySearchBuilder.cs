﻿using PortalGuitarFlash.DDD.Enum;
using System.Collections.Generic;
using System.Linq;

namespace PortalGuitarFlash.DDD.Interfaces
{
    public interface IEntitySearchBuilder<TEntity>
    {
        string SearchText { get; set; }
        SortingType SortingOrder { get; set; }
        IOrderedEnumerable<TEntity> Build(IEnumerable<TEntity> entities);

    }
}
