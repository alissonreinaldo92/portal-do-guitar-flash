﻿using PortalGuitarFlash.DDD.Helpers;
using System;
using System.Collections.Generic;

namespace PortalGuitarFlash.DDD.Interfaces.Repositories
{
    public interface IBaseRepository<TEntity>: IDisposable where TEntity: class
    {

        void Add(TEntity obj, bool SaveChanges = true);
        TEntity GetById(int ID);
        IEnumerable<TEntity> GetAll();
        CollectionPage<TEntity> GetAllPaged(int page, int itemsPerPage, IEntitySearchBuilder<TEntity> orderer);
        void Update(TEntity obj, bool SaveChanges = true);
        void Remove(TEntity obj, bool SaveChanges = true);

    }
}
