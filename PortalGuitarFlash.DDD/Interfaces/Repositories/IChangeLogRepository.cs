﻿using PortalGuitarFlash.DDD.Entities;

namespace PortalGuitarFlash.DDD.Interfaces.Repositories
{
    public interface IChangeLogRepository : IBaseRepository<ChangeLog>
    {
    }
}
