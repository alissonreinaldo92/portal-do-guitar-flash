﻿using PortalGuitarFlash.DDD.Entities;

namespace PortalGuitarFlash.DDD.Interfaces.Repositories
{
    public interface IDownloadedSongRepository : IBaseRepository<DownloadedSong>
    {
    }
}
