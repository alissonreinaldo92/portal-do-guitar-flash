﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalGuitarFlash.DDD.Enum
{

    public enum ImageType
    {
        Profile,
        Background
    }
    public enum SortingType
    {
        Ascending,
        Descending
    }
}
